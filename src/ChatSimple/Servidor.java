package ChatSimple;

import java.net.*;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Servidor {

    ArrayList<Conexion> conexiones;
    ArrayList<String> usuariosActivos;
    ServerSocket ss;

    String[][] usuarios = { {"Hugo", "123"},
                            {"Paco", "345"},
                            {"Luis", "890"},
                            {"Donald", "678"}};
  
    public static void main(String[] args) {
        // TODO code application logic here
        java.awt.EventQueue.invokeLater((new Servidor())::start);
    }

    private void start() {
        this.conexiones = new ArrayList<>();
        this.usuariosActivos = new ArrayList<String>();
        Socket socket;
        Conexion cnx;
       
        try {
            ss = new ServerSocket(4444);
            System.out.println("Servidor iniciado, en espera de conexiones");

            while (true) {
                socket = ss.accept();
                cnx = new Conexion(this, socket, conexiones.size());
                conexiones.add(cnx);
                cnx.start();
            }

        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Broadcasting
    public void difundir(String id, String mensaje) {
        Conexion hilo;
        for (int i = 0; i < this.conexiones.size(); i++) {
            hilo = this.conexiones.get(i);
            if (hilo.cnx.isConnected()) {
                if (!id.equals(hilo.id)) {
                    hilo.enviar(mensaje);
                }
            }
        }//To change body of generated methods, choose Tools | Templates.
    }

    class Conexion extends Thread {
        private Connection bd = null;
        BufferedReader in;
        PrintWriter out;
        Socket cnx;
        Servidor padre;
        int numCnx = -1;
        String id = "";

        public final int SIN_USER = 0;
        public final int USER_IDENT = 1;
        public final int PASS_PDTE = 2;
        public final int PASS_OK = 3;
        public final int CHAT = 4;

        public Conexion(Servidor padre, Socket socket, int num) {
            this.cnx = socket;
            this.padre = padre;
            this.numCnx = num;
            this.id = socket.getInetAddress().getHostAddress() + num;
        }

        @Override
        public void run() {
            String linea = "", user = "", pass = "", mensaje = "";
            int estado = SIN_USER;
            int usr = -1;

            try {
                in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
                out = new PrintWriter(cnx.getOutputStream(), true);

                System.out.printf("Aceptando conexion desde %s\n",
                        cnx.getInetAddress().getHostAddress());
                while (!mensaje.toLowerCase().equals("salir")) {
                    switch (estado) {
                        case SIN_USER:
                            //out.println("Bienvenido, proporcione su usuario");
                            estado = USER_IDENT;
                            break;
                        case USER_IDENT:
                            user = in.readLine();
                            boolean found = false;
                            for (int i = 0; i < usuarios.length; i++) {
                                if (user.equals(usuarios[i][0])) {
                                    found = true;
                                    usr = i;
                                }
                            }
                            System.out.println(found);
                            if (!found) {
                                estado = SIN_USER;
                                out.println("No Autenticado");
                                mensaje = "Salir";
                            } else {
                                estado = PASS_PDTE;
                            }
                            break;
                        case PASS_PDTE:
                            //out.println("Escriba el password");
                            pass = in.readLine();

                            if (pass.equals(usuarios[usr][1])) {
                                estado = PASS_OK;
                            }else{
                                out.println("No Autenticado");
                            }
                            break;
                        case PASS_OK:
                            out.println("Autenticado!");
                            estado = CHAT;
                            this.padre.difundir(this.id, user + " se ha unido al chat");
                            //Se agrega el nuevo usuario a los activos
                            usuariosActivos.add(user);
                            String users = "";
                            for(int i = 0; i < usuariosActivos.size(); i++){
                                users += usuariosActivos.get(i) + ",";
                            }
                            //Se guarda en una string y se manda a todos
                            this.padre.difundir(this.id,"/" + users);
                            //Tambien se manda a el nuevo usuarios
                            enviar("/" + users);
                            break;
                        case CHAT:
                            mensaje = in.readLine();
                            System.out.printf("%s - %s\n",
                                    cnx.getInetAddress().getHostAddress(),
                                    user + ":" + mensaje);

                            this.padre.difundir(this.id, user + " : " + mensaje);
                            break;
                    }
                }
                System.out.printf("%s - %s\n",
                                    cnx.getInetAddress().getHostAddress(),
                                    user + " ha salido del chat.");
                if(estado == CHAT){
                    this.padre.difundir(this.id, user + " ha salido del chat.");
                    //Quitar el usuario de la lista de usuarios activos
                    usuariosActivos.remove(user);
                    //Guardar los activos en una cadena para despues enviarla
                    String users = "";
                    for(int i = 0; i < usuariosActivos.size(); i++){
                        users += usuariosActivos.get(i) + ",";
                    }
                    //Se manda con una /, para que del lado del cliente la pueda reconocer y guardar aparte
                    this.padre.difundir(this.id,"/" + users);
                }
                
                this.cnx.close();
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        private void enviar(String mensaje) {
            this.out.println(mensaje); //To change body of generated methods, choose Tools | Templates.

        }
        private boolean obtenerUsuario(String usuario){
            boolean encontrado = false;
            PreparedStatement stmt;
            String query;
            //Cambiar en ambos metodos lo que es el nombre de los campos que tienes en tu base de datos
            query = String.format("SELECT usuario FROM users WHERE usuario='%s'", usuario);
            try {
                if (bd == null)
                    bd = DriverManager.getConnection("jdbc:mysql://localhost:3306/encuesta?" + "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&user=encuesta&password=encuesta");
                stmt = bd.prepareStatement(query);
                ResultSet rs = stmt.executeQuery();
                encontrado = rs.next();
            } catch (SQLException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }
            return encontrado;
        }
        private boolean obtenerContra(String usuario, String password){
            boolean encontrado = false;
            PreparedStatement stmt;
            String query;
            query = String.format("SELECT usuario FROM users WHERE usuario='%s' AND password='%s'", usuario, password);
            try {
                if (bd == null)
                    bd = DriverManager.getConnection("jdbc:mysql://localhost:3306/encuesta?" + "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&user=encuesta&password=encuesta");
                stmt = bd.prepareStatement(query);
                ResultSet rs = stmt.executeQuery();
                encontrado = rs.next();
            } catch (SQLException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }
            return encontrado;
        }
    }
}
