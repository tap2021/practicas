package Practicas;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Practica9 {
    int y = 0;
    
    public static void main(String[] args) {
        HiloT  h1 = new HiloT("P1",10,2000);
        Thread h2 = new Thread(new HiloR("P2",20,1500));
        
        h1.start();
        h2.start();
        
        System.out.println("Proceso main terminó");
    }
}

class HiloT extends Thread{
    String id = "";
    int x = 0;
    int tiempo = 0;
    
    HiloT (String id, int x, int tiempo){
        this.x = x;
        this.id = id;
        this.tiempo = tiempo;
    }
            
    public void run(){
        for(int i=0; i<x; i++){
            System.out.printf("Hilo: %s - i = %d\n",id,i);
            try {
                Thread.sleep(this.tiempo);
            } catch (InterruptedException ex) {
                Logger.getLogger(Practica9.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
    
    class HiloR implements Runnable{
    String id = "";
    int x = 0;
    int tiempo = 0;
    
    HiloR (String id, int x, int tiempo){
        this.x = x;
        this.id = id;
        this.tiempo = tiempo;
    }
            
    public void run(){
        for(int i=0; i<x; i++){
            System.out.printf("Hilo: %s - i = %d\n",id,i);
            try {
                Thread.sleep(this.tiempo);
            } catch (InterruptedException ex) {
                Logger.getLogger(Practica9.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}